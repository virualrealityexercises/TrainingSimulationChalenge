using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class TeleportController : MonoBehaviour
{
    [SerializeField] private InputActionReference teleportActivationReference;
    private XRInteractorLineVisual xrInteractorLineVisual;
    private void Start() 
    {
        xrInteractorLineVisual = GetComponent<XRInteractorLineVisual>();
        //xrInteractorLineVisual.reticle.SetActive(false);
        xrInteractorLineVisual.enabled = false;
    }

    private void Update() 
    {
        
    }
    private void OnEnable() {
        teleportActivationReference.action.performed += TeleportAction_performed;
        teleportActivationReference.action.canceled += TeleportAction_canceled;
    }   

    private void OnDisable() {
        teleportActivationReference.action.performed -= TeleportAction_performed;
        teleportActivationReference.action.canceled -= TeleportAction_canceled;
    }

    private void TeleportAction_performed(InputAction.CallbackContext context)
    {
        if (context.action.ReadValue<float>() > 0) 
        {
            xrInteractorLineVisual.enabled = true;
            //xrInteractorLineVisual.reticle.SetActive(true);
        }
    }

    private void TeleportAction_canceled(InputAction.CallbackContext context)
    {
        if (context.action.ReadValue<float>() <= 0) 
        {
            //xrInteractorLineVisual.reticle.SetActive(false);
            xrInteractorLineVisual.enabled = false;
        }
    }
}

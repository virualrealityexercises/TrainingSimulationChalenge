using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondHand : ClockHand
{    
    public SecondHand(GameObject clockSecondHand) : base(clockSecondHand) {}    
    public void SetInitialGrades(int gradesPerSecond)
    {
        InitialGrades = System.DateTime.Now.Second * gradesPerSecond;
        ActualGrades = InitialGrades;
    }
    new public void RotateHandToInitialPosition()
    {
        base.RotateHandToInitialPosition();
    }

    public void UpdateSeconds(int grades)
    {
        base.UpdateHand(grades);
    }
}

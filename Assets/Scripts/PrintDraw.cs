using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintDraw : MonoBehaviour
{
    [SerializeField]
    private CreateTrail trail;
    [SerializeField]
    private GameObject spawnPosition;

    private List<GameObject> trailObjectsList;
    private List<GameObject> printedObjectsList;

    private void Awake() {
        printedObjectsList = new List<GameObject>();
    }

    public void PrintDrawTrail()
    {
        trailObjectsList = trail.GetTrailObjects();

        if(printedObjectsList.Count > 0){
            printedObjectsList.Clear();
        }

        if(trailObjectsList.Count > 0)
        {
            foreach (GameObject trail in trailObjectsList)
            {
                // Transform trailTransform = trail.transform;

                // TrailRenderer trailRenderer = trail.GetComponent<TrailRenderer>();
                // MeshFilter meshFilter = trail.AddComponent<MeshFilter>();
                // Mesh mesh = new Mesh();
                // trailRenderer.BakeMesh(mesh);
                // meshFilter.sharedMesh = mesh;

                // MeshRenderer meshRenderer = trail.AddComponent<meshRenderer>();
                //Debug.LogWarning("currenTrail OK");
                //float dist = Vector3.Distance(spawnPosition.transform.position, trailTransform.position);
                // Vector3 newScale = new Vector3((trailTransform.localScale.x * 0.25f),
                //                             (trailTransform.localScale.y * 0.25f),
                //                             (trailTransform.localScale.z * 0.25f));
                //Debug.LogWarning("Scale = "+trailTransform.localScale);
                //Debug.LogWarning("New Scale = "+newScale);

                //trail.transform.localScale = newScale;
                //Debug.LogWarning("New Scale = "+trail.transform.localScale);

                // Vector3 newPos = new Vector3((spawnPosition.transform.position.x + dist),
                // (spawnPosition.transform.position.y + dist), (spawnPosition.transform.position.z + dist));
                // trail.transform.position = newPos;

                // printedObjectsList.Add(trail);
            }
        } else {
            Debug.LogWarning("trailObjectsList = null");
        }

        
    }
}

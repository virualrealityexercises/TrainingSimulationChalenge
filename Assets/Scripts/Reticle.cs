using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reticle : MonoBehaviour
{
    private const float DEGRES_X_SECOND = 70;
     
    private void Update() 
    {
        transform.Rotate(new Vector3(0,DEGRES_X_SECOND,0) * Time.deltaTime);    
    }
}

﻿using UnityEngine;

/// <summary>
/// This script makes it easier to toggle between a new material, and the objects original material.
/// </summary>
public class ChangeMaterial : MonoBehaviour
{
    [SerializeField] Material clear;
    [SerializeField] Material mirror;

    private Renderer renderer;

    private void Start() {
        renderer = GetComponent<Renderer>();
        renderer.material = clear;
    }

    public void SetMatClear()
    {
        renderer.material = clear;
    }

    public void SetMatMirror()
    {
        renderer.material = mirror;
    }
}

using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This script creates a trail at the location of a gameobject with a particular width and color.
/// </summary>

public class CreateTrail : MonoBehaviour
{
    public GameObject trailPrefab = null;

    private float width = 0.05f;
    private Color color = Color.white;
    private GameObject currentTrail = null;
    private List<GameObject> trailObjects;

    private void Awake() {
        trailObjects = new List<GameObject>();
    } 

    public void StartTrail()
    {
        if (!currentTrail)
        {
            currentTrail = Instantiate(trailPrefab, transform.position, transform.rotation, transform);
            ApplySettings(currentTrail);            
        }
    }

    private void ApplySettings(GameObject trailObject)
    {
        TrailRenderer trailRenderer = trailObject.GetComponent<TrailRenderer>();
        trailRenderer.widthMultiplier = width;
        trailRenderer.startColor = color;
        trailRenderer.endColor = color;
    }

    public void EndTrail()
    {
        trailObjects.Add(currentTrail);

        if (currentTrail)
        {
            currentTrail.transform.parent = null;
            currentTrail = null;
        }
    }


    public void CleanTrailObjects() {
        trailObjects.Clear();
    }

    public void SetWidth(float value)
    {
        width = value;
    }

    public void SetColor(Color value)
    {
        color = value;
    }

    public List<GameObject> GetTrailObjects()
    {
        return trailObjects;
    }
}
